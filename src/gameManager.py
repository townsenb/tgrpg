import time
import random
import time
from bandit import Bandit
import bandit
from locationGen import Location
import changelog
import mapGen
import direction
import locType
from nameGen import nameGen
import storyText
from tips import Tips
import container
import spell
import item
import itemManager
from character import Character
import character
from weapon import Weapon, WeaponType
from armor import Armor
import weapon
import quest


class GameManager():

    def log(self,string):
        log_behavior = 'no'
        if log_behavior == 'print':
            print(string)
        elif log_behavior == 'log':
            pass
        return


    def text(self,string):
        if string == '' or string == None:
            return
        if self.test_mode:
            print(f"\n{('_'*80)}\n{string}\n{('_'*80)}\n")
        else:
            self.bot.sendMessage(text=string,chat_id=self.chat_id)


    def newCampaign(self):

        self.world = None
        self.playerDict = {}
        self.currentLoc = None
        self.inBattle = False
        self.enemyList = []
        self.turnOrder = []
        self.loggedout = {}
        self.turn = 0
        self.battleTimeout = 0
        
        self.text("generating...")
        world = mapGen.Map()
        self.currentLoc = world.startLocation
        worldsize = 7
        world.genMap(self.currentLoc,worldsize)
        # don't let it random generate something too small
        while(world.totalSites <= 100):   
            self.log("sites: " + str(world.totalSites))
            world.genMap(self.currentLoc,worldsize)
        self.world = world
        self.text("{} sites generated\n\n{}".format(world.totalSites,world.getSitesCount()))
        self.text("Welcome to the land of {}!\n\nCreate a character with /dm newcharacter [name] (race)".format(world.name))
        direct = "\n".join(mapGen.getConnections(self.currentLoc))


    def newCharacter(self,user,name,race='human'):
        if self.inBattle:
            self.text("Wait until the battle finishes, buster!")
            return
        if len(name) > 20:
            self.text("Name too long, buster!")
            return
        race = race.lower()
        if race not in character.races:
            self.text(f"{race} is not a valid race!\n(human, dwarf, elf, larchling, orc, goblin)")
            return
       
        tmpCharacter = Character(name.lower().title())
        tmpWeap = weapon.new('bronzeknife')
        tmpCharacter.weapons.append(tmpWeap)
        tmpCharacter.equip = tmpWeap
        tmpCharacter.player = user
        tmpCharacter.race = race
        if race == 'dwarf':
            tmpCharacter.strengthStat += 2
            tmpCharacter.agilityStat -= 2
            tmpCharacter.magicStat -= 2
        elif race == 'elf':
            tmpCharacter.magicStat += 2
            tmpCharacter.agilityStat -= 2
            tmpCharacter.strengthStat -= 2
            tmpCharacter.items.append(item.items['spellbookofzap'])
        elif race == 'larchling':
            tmpCharacter.agilityStat += 2
            tmpCharacter.magicStat -= 2
            tmpCharacter.strengthStat -= 2
        elif race == 'orc':
            tmpCharacter.strengthStat += 4
            tmpCharacter.agilityStat -= 2
            tmpCharacter.magicStat -= 4
        elif race == 'goblin':
            tmpCharacter.strengthStat -= 4
            tmpCharacter.agilityStat += 4
            tmpCharacter.magicStat -= 2
        tmpCharacter.resetStats()
        tmpCharacter.description = f'{name} the {tmpCharacter.race} is a player character controlled by {user}'
        self.playerDict[user] = tmpCharacter
        self.text(random.choice(storyText.newlyMadeCharacter).format(tmpCharacter.name,locType.prettyLocType(self.currentLoc.type),self.currentLoc.name,tmpCharacter.gold,tmpCharacter.weapons[0]))    
        self.text("Directions:\n" + "\n".join(mapGen.getConnections(self.currentLoc)))
        self.playerDict[user].knownLocs.append(self.currentLoc)

        if self.test_mode:
            tmpCharacter.spells.append(spell.spells['kill'])
            tmpCharacter.gold = 1000000


    def charStats(self,c):
        statsStr = "{}\n\nLevel: {}\nExp: {}/{}\nGold: {}\n\nHealth: {}/{}".format(c.name,c.level,c.xp,c.level*c.XP_PER_LEVEL,c.gold,c.currentHealth,c.healthStat)
        statsStr += "\nStrength: {}/{}\nMagic: {}/{}\nAgility: {}/{}\n".format(c.tmpStrength,c.strengthStat,c.tmpMagic,c.magicStat,c.tmpAgility,c.agilityStat)
        return statsStr

    def bountyCheck(self,c):
        if c.bounty > 0:
            self.text(f"{c.name} has a bounty. Best not draw any attention to yourself.")
            return True
        else:
            return False

    def inventory(self,c):
        weapNames = []
        for weap in c.weapons:
            wstr = ''
            if c.equip == weap:
                wstr += '* ' 
            wstr += weap.name
            weapNames.append(wstr)

        spells = [sp.name for sp in c.spells]
        sep = "\n" + (" " * 8) 
        invStr = "\nGold: {}\nWeapons: {}{}\nSpells: {}{}\nItems: {}{}".format(c.gold,sep,sep.join(weapNames),sep,sep.join(spells),sep,sep.join(c.getItemStrCondensed()))
        return invStr


    def directions(self,char):
        dirstr = "\n".join(mapGen.getConnections(self.currentLoc))
        if self.currentLoc.type == locType.LocType.CITY or self.currentLoc.type == locType.LocType.VILLAGE:
            randNpc = random.choice(self.currentLoc.inhabitants)
            resultstr = "You encounter a local named {} who points out nearby landmarks.\n".format(randNpc.firstname) + dirstr
        else:
            resultstr = "The signpost at a nearby crossroads shows you the way.\n" + dirstr
        return resultstr

    def travel(self,c,direct):
        if direct == None or direct == '':
            return "Please specify a valid direction!"
        dirint = direction.parseDirection(direct.lower())
        if dirint < 0 or dirint >= 8 or self.currentLoc.roads[dirint] == None:
            return "Please specify a valid direction!"
        dist = self.currentLoc.roads[dirint].distance
        banditChance = dist * random.randint(1,5)
        self.currentLoc = self.currentLoc.roads[dirint].connections[1]
        for char in self.playerDict:
            if self.currentLoc not in self.playerDict[char].knownLocs: self.playerDict[char].knownLocs.append(self.currentLoc)
        self.log(f"smush: {self.currentLoc.smushName}")
        self.text("you have arrived in the {} of {} after {} hours of walking".format(locType.prettyLocType(self.currentLoc.type),self.currentLoc.name,dist))
        if self.currentLoc.type == locType.LocType.CAMP or self.currentLoc.type == locType.LocType.CAVE:
            if banditChance > 10:
                return self.banditAttack()
            else:
                return f"this {locType.prettyLocType(self.currentLoc.type)} looks abandonded"

    def fasttravel(self,c,smushName='history'):
        if smushName == 'history':
            output = 'You recall all the places you have visited:\n\n'
            for place in c.knownLocs:
                output += f"{place.name} - {locType.prettyLocType(place.type)}\n"
            self.text(output)
            return
        dest = None
        smushName = smushName.lower()
        oldLoc = self.currentLoc
        for loc in c.knownLocs:
            if smushName == loc.smushName:
                self.log(f'loc found: {loc.name}')
                dest = loc
        if dest == None:
            return "improper smush name, try again!"
        
        self.currentLoc = dest
        return "you have arrived in the {} of {} after a long journey from {}".format(locType.prettyLocType(self.currentLoc.type),self.currentLoc.name,oldLoc.name)


    def shop(self,c):
        if not self.currentLoc.hasShop:
            return "There is no shop here!"
        if self.bountyCheck(c): return
        return storyText.shop.format(self.currentLoc.shop.shopKeeper.firstname, c.name,self.currentLoc.shop.getShopInv())


    def buy(self,c,itemName):        
        widget = None
        if not self.currentLoc.hasShop:
            return "There is no shop here!"

        if self.bountyCheck(c): return

        store = self.currentLoc.shop.shopInventory
        itemName = itemName.lower()
        for thing in store:
            if itemName == thing.smushName:
                self.log(f'item found: {thing.name}')
                widget = thing
        if widget == None:
            return '"Sorry, {}. We don\'t have that here."'.format(c.name)
        if widget.localPrice > c.gold:
            return storyText.notEnoughGold.format(self.currentLoc.shop.shopKeeper.firstname,c.name)

        #successful purchase
        widget.transferOwner(c)
        shopkeep = self.currentLoc.shop.shopKeeper
        self.currentLoc.shop.shopInventory.remove(widget)
        if widget in shopkeep.items: shopkeep.items.remove(widget)
        if widget in shopkeep.weapons: shopkeep.weapons.remove(widget)
        c.gold -= widget.localPrice
        return storyText.bought.format(self.currentLoc.shop.shopKeeper.firstname,c.name,widget.name,widget.localPrice)


    def sell(self,c,itemName,amount='1'):
        if not amount.isdigit():
            return "bad input - /dm sell [item] (amount)"
        amount = int(amount)

        if self.bountyCheck(c): return  

        if not self.currentLoc.hasShop:
            self.text("There is no shop here!")
            return
        if amount <= 0:
            return "well ok then. bye!"

        itemName = itemName.lower()
        store = self.currentLoc.shop.shopInventory
        widgetName = ''
        tab = 0
        sales = 0
        for i in range(0,amount):
            widget = None
            for thing in c.getSellables():
                if itemName == thing.getSmushName():
                    widget = thing
            if widget == None:
                self.text(f'You don\'t have {amount} in your inventory, {c.name}!')
                if tab > 0:
                    self.text(f'I\'m buying the ones you do have though!')
                    return storyText.sold.format(self.currentLoc.shop.shopKeeper.firstname,article,widgetName,plural,c.name,tab)

                else:
                    return

            #successful sale
            widget.transferOwner(self.currentLoc.shop.shopKeeper)
            widget.localPrice = widget.value + self.currentLoc.affluence
            store.append(widget)
            if widget in c.items: c.items.remove(widget)
            if widget in c.weapons: c.weapons.remove(widget)
            c.gold += widget.value
            tab += widget.value
            sales += 1
            article = "that" if sales == 1 else "those"
            plural = "s" if sales > 1 else ""
            widgetName = widget.name
        return storyText.sold.format(self.currentLoc.shop.shopKeeper.firstname,article,widgetName,plural,c.name,tab)


    def bet(self,c,betAmt,guess):
        if not betAmt.isdigit():
            return "bad input - /dm bet [amount] [heads|tails]"
        if not self.currentLoc.hasShop:
            return "No one around here wants to gamble."
        betAmt = int(betAmt)
        if betAmt > c.gold or betAmt < 1:
            return "You tryin to swindle me, bud?"
        guessHeads = False
        guess = guess.lower()
        if guess == 'heads' or guess == 'h':
            guessHeads = True
        elif guess == 'tails' or guess == 't':
            guessHeads = False
        else:
            return "bad input - /dm bet [amount] [heads|tails]"
        flip = random.choice([True,False])
        side = "Heads" if flip else "Tails"
        self.text(side)
        if guessHeads == flip:
            c.gold += betAmt
            return "You won! {} recieves {} gold!".format(c.name,betAmt)
        else:
            c.gold -= betAmt
            return "You lost. Better luck next time, eh {}? I'll just take that {} gold from ya...".format(c.name,betAmt)


    def search(self,c):
        loc = self.currentLoc
        searchSet = None
        
        if loc.type == locType.LocType.VILLAGE:
            searchSet = storyText.villageSearchables
        elif loc.type == locType.LocType.CITY:
            searchSet = storyText.citySearchables
        elif loc.type == locType.LocType.CAVE:
            searchSet = storyText.caveSearchables
        elif loc.type == locType.LocType.CAMP:
            searchSet = storyText.campSearchables
        
        success = True if random.randint(0,100) < 66 else False

        if loc.hasCorpse:
            bodyList = self.currentLoc.getCorpses()
            self.currentLoc.hasCorpse = False
            return storyText.afterFightSearchables['succeed'][0].format(c.name,bodyList)
        
        if self.currentLoc.beenSearched == True:
            return "This area has already been searched"

        if self.currentLoc.questItemCount > 0:
            findQuest = None
            for q in c.quests:
                if q.type == quest.QuestType.FIND and q.isActive:
                    findQuest = q
                    break
            if findQuest != None:
                c.items.append(findQuest.item)
                self.currentLoc.questItemCount -= 1
                findQuest.isActive = False
                where = random.choice(storyText.foundWhere)
                return f"{c.name} finds {findQuest.giver}'s {findQuest.item.name} {where}"


        self.currentLoc.beenSearched = True
        if success:
            containerType = random.choices(list(container.ContainerType),weights=[5,2,1],k=1)[0]
            containerName = random.choice(container.containerNames[containerType])
            self.currentLoc.containers.append(container.Container(containerType,containerName))
            return searchSet['succeed'][containerType][0].format(c.name,containerName)
        else:
            return searchSet['fail'][0].format(c.name)
        


    def loot(self,c,itemName):
        itemName = itemName.lower()
        loot = None
        for thing in self.currentLoc.getLootables():
            if itemName == thing.getSmushName():
                loot = thing
        if loot == None:
            return "There is nothing around here with that name that you can loot"

        
        if type(loot) == Character:
            if loot.pickpocketed:
                return "This person has already been pickpocketed"
            loot.pickpocketed = True
            lootChance = random.randint(0,50)
            if lootChance <= c.agilityStat:
                if len(loot.items) == 0:
                    c.gold += loot.gold
                    return f"{c.name} successfully pickpockets {loot.name} and steals {loot.gold} Gold."
                else:
                    c.gold += loot.gold
                    for item in loot.items:
                        if type(item) is Weapon:
                            c.weapons.append(weapon.new(item.getSmushName()))
                        else:
                            c.items.append(item)
                    return f"{c.name} successfully pickpockets {loot.name} and steals {loot.getItemList()}and {loot.gold} Gold."
            else:
                c.bounty += 10
                return "Keep your hands to yourself you thieving vagrant!\n(+10 G bounty)"

        else:
            prefix = ''
            self.currentLoc.containers.remove(loot)
            if loot.isCorpse: 
                    prefix = 'body of '
                    if loot in self.currentLoc.corpses:
                        self.currentLoc.corpses.remove(loot)
                    if len(self.currentLoc.corpses) == 0:
                        self.currentLoc.hasCorpse = False

            if len(loot.items) == 0:
                c.gold += loot.gold
                return f"{c.name} loots the {prefix}{loot.name} and finds {loot.gold} Gold."
            else:
                c.gold += loot.gold
                for item in loot.items:
                    if type(item) is Weapon:
                        c.weapons.append(weapon.new(item.getSmushName()))
                    else:
                        c.items.append(item)
                return f"{c.name} loots the {prefix}{loot.name} and finds {loot.getItemList()}and {loot.gold} Gold."


    def equip(self,c,weaponName):
        equip = None
        weaponName = weaponName.lower()
        for weap in c.weapons:
            if weaponName == weap.getSmushName():
                if weap.type == WeaponType.BOW:
                    if weap.minLevel > c.agilityStat:
                        return f"You can't use this yet! (need {weap.minLevel} agility)"
                else:
                    if weap.minLevel > c.strengthStat:
                        return f"You can't use this yet! (need {weap.minLevel} strength)"
                equip = weap
                c.equip = equip

        for armor in c.items:
            if type(armor) == Armor and weaponName == armor.getSmushName():
                if armor.minLevel > c.strengthStat:
                    return f"You can't use this yet! (need {armor.minLevel} strength)"
                equip = armor
                c.armorEquip = armor

        if equip is None:
            return "You don't have any weapons or armor with that name"
        
        if equip.name == "Blade of Dwelako":
            self.text("As you lift the legendary blade towards the sky, you feel an overwhelming feeling of achievement. You feel complete.")
        if equip.name == "Dwelakan Chainmail":
            self.text("You don the mythic armor. The fine yet sturdy chains are of unknown metal, but feel like cold silk on your skin. You feel incredible.")
        return f"{c.name} equipped their {equip.name}"


    def banditAttack(self):
        allies = list(self.playerDict.values())
        avglevel = 0
        for p in allies:
            avglevel += p.level
        avglevel = int(avglevel / len(allies))
        print(avglevel)
        enemies = []
        for i in range(0,max(1,len(allies) + random.randint(-2,1))): #generate enemies
            tmpEnemy = Bandit(nameGen(random.randint(2,7)).title(),random.randint(max(avglevel-1,1),avglevel+1))
            tmpEnemy.gold = random.randint(0,20) * tmpEnemy.level
            tmpEnemy.currentHealth = tmpEnemy.maxHealth - random.randint(0,3)
            enemies.append(tmpEnemy)
            self.enemyList.append(tmpEnemy)
        self.initiateBattle(allies,enemies)
        banditNames = ''
        for b in enemies:
            banditNames += f"{b.name}, "
        self.text(f"{len(enemies)} bandits are approaching!\n\n{banditNames} are initiating a battle!")
        self.battleTurn()


    def challenge(self,c,npcName):
        npc = None
        for n in self.currentLoc.inhabitants:
            if n.getSmushName() == npcName.lower():
                npc = n
        if npc == None:
            return "There is no one here with that name"

        bountyMessage = ''
        allies = list(self.playerDict.values())
        avglevel = 0
        for p in allies:
            if self.currentLoc.type == locType.LocType.CITY or self.currentLoc.type == locType.LocType.VILLAGE:
                p.bounty += 50
                bountyMessage = "\n(+50 G bounty for everyone)"
            avglevel += p.level
        avglevel = int(avglevel / len(allies))
        enemies = [npc]
        for n in self.currentLoc.inhabitants:
            if n != npc and random.randint(0,2) == 0: #1/3 chance of local joining battle
                enemies.append(n)
        
        enemyNameList = ''
        for enemy in enemies:
            enemyNameList += f"{enemy.firstname}, "
            enemy.level = avglevel + max(1,random.randint(-1,1))
            enemy.npcLevelUp()
            enemy.resetStats()
            bandit.getWeaponByLevel(enemy)
            bandit.getArmorByLevel(enemy)
            self.enemyList.append(enemy)
        
        self.initiateBattle(allies,enemies)
        self.text(f"{c.name} challenges {npc.firstname} to a battle!{bountyMessage}")
        self.text(f"{enemyNameList} join the fight to protect {npc.firstname}")
        self.battleTurn()   


    def initiateBattle(self,playerList,enemyList):
        allBattlers = playerList + enemyList
        while len(self.turnOrder) < len(allBattlers):
            r = random.randint(0,len(allBattlers)-1)
            if allBattlers[r] not in self.turnOrder:
                self.turnOrder.append(allBattlers[r])

        # sort list in order of agility
        self.turnOrder = sorted(self.turnOrder, key=lambda x: x.agilityStat, reverse=True)
        self.turn = -1
        self.inBattle = True



    def battleUpdate(self,c):
        for char in self.turnOrder:
            char.statDesc = char.getStatStr()

            if char.currentHealth <= 0:
                xpGain = int((char.maxHealth / len(self.playerDict)) * 1.5)
                self.text(f"{char.name} has died by {c.name}'s hand\nEveryone gains {xpGain} xp!")
                # distribute xp
                for c in self.turnOrder:
                    if c not in self.enemyList:
                        c.xp += xpGain
                if self.turnOrder.index(char) < self.turn:
                    self.turn -= 1
                self.turnOrder.remove(char)
                if char in self.playerDict.values():
                    loser = list(self.playerDict.keys())[list(self.playerDict.values()).index(char)]
                    self.text(f"RIP {char.name}...\n{loser} make a new character I guess 😬")
                    for pc in self.playerDict:
                        if self.playerDict[pc].name == char.name:
                            del self.playerDict[pc]
                            break

                else:
                    self.enemyList.remove(char)
                    allies = [p for p in self.turnOrder if p not in self.enemyList]
                    quest.killedTarget(allies,char)
                    print("QUEST COMPLETED")

                
                # drop inventory to container
                self.currentLoc.hasCorpse = True
                tmpCont = container.Container(container.ContainerType.MEDIUM,char.name)
                tmpCont.gold = char.gold
                tmpCont.items = []
                tmpCont.isCorpse = True
                self.currentLoc.corpses.append(tmpCont)
                for i in char.items:
                    tmpCont.items.append(i)
                for w in char.weapons:
                    tmpCont.items.append(w)

                self.currentLoc.containers.append(tmpCont)

        if len(self.enemyList) == 0:
            self.enemyList = []
            self.inBattle = False
            self.turnOrder = []
            self.text(f"You won the battle!")
            return
        
        allies = [p for p in self.turnOrder if p not in self.enemyList]
        if len(allies) == 0 and len(self.loggedout.keys()) == 0:
            self.text("Everyone has perished.\nA new land in Tegramalon emerges from the ashes in 10s.")
            time.sleep(10)
            self.newCampaign()
        
        if len(self.loggedout.keys()) > 0:
            self.text("The party has lost this battle. The bandits run off with your stuff.")
            self.enemyList = []
            self.inBattle = False
            self.turnOrder = []
            self.currentLoc.hasCorpse = False
            return



    def battleTurn(self):
        if not self.inBattle: return
        self.turn += 1
        self.turn %= len(self.turnOrder)
        turntaker = self.turnOrder[self.turn]
        self.battleTimeout = int(time.time()) + 60
        print(self.battleTimeout)
        self.text(f"{turntaker.name}'s turn ({turntaker.currentHealth}/{turntaker.maxHealth} HP)")
        if turntaker not in list(self.playerDict.values()): # turn taker is enemy
            time.sleep(3)
            self.text(self.battleAI(turntaker))
            time.sleep(2)
            self.battleTurn()
            return
        return


    def skipturn(self,c):
        self.text(f"{c.name} skips their turn")
        self.battleTurn()
        return
    
    def battleStats(self,c):
        friendlies = [self.playerDict[p].name for p in self.playerDict]
        result = 'Turn Order: \n\n'
        for char in self.turnOrder:
            who = ''
            if self.turnOrder[self.turn] == char:
                result += '* '
            if char.name in friendlies:
                who = f'- ({char.player})'
            result += f'{char.name} - {char.currentHealth}/{char.maxHealth} {who}\n'

        self.inBattle = True
        return result


    def applySpellEffect(self, effects,target,effectValue):
        if spell.Effectables.HEALTH in effects: 
            target.currentHealth = min(target.currentHealth + effectValue, target.maxHealth)
        if spell.Effectables.STRENGTH in effects: 
            target.tmpStrength = min(target.tmpStrength + effectValue, target.strength)
        if spell.Effectables.MAGIC in effects: 
            target.tmpMagic = min(target.tmpMagic + effectValue, target.magic) 
        if spell.Effectables.AGILITY in effects: 
            target.tmpAgility = min(target.tmpAgility + effectValue, target.agility)


    def cast(self,c,spellName,targetName):
        spellName = spellName.lower()
        targetName = targetName.lower()
        spellcast = None
        for sp in c.spells:
            if spellName == sp.name.lower():
                spellcast = sp
        if spellcast is None:
            return "You don't know a spell by that name"

        target: Character = None
        for char in self.turnOrder:
            if targetName.lower() == char.getSmushName().lower():
                target = char
        if target is None:
            return "Nobody on the battlefield is named that"

        #successful cast
        if c.tmpMagic >= spellcast.cost:
            effectValue = random.randint(spellcast.min,spellcast.max)
            c.tmpMagic -= spellcast.cost
            self.applySpellEffect([spellcast.effect],target,effectValue)
            valueDirection = 'loses' if effectValue < 0 else 'gains'
            self.text(f"{c.name} cast {spellcast.name} on {target.name}, who {valueDirection} {abs(effectValue)} {spell.prettyEffectable[spellcast.effect]}")
            self.battleUpdate(c)
            if self.inBattle:
                self.battleTurn()
            return
        
        return f"{c.name} doesn't have enough magic to cast this spell (needs {spellcast.cost})"


    def attack(self,c:Character,targetName):
        targetName = targetName.lower()
        target: Character = None
        for char in self.turnOrder:
            if targetName == char.getSmushName().lower():
                target = char
        if target is None:
            return "Nobody on the battlefield is named that"
        
        #successful hit
        weap = c.equip
        stat = 0
        if weap.type == WeaponType.BLADE or weap.type == WeaponType.BLUNT:
            stat = c.strength #melee
        else:
            stat = c.agility #bows

        crit = True if random.randint(0,10) == 0 else False
        dmg = max(0,int(weap.damageMult * random.randint(stat-2,stat+2)) - target.armorEquip.damageReduce)
        dmg *= 2 if crit else 1
        target.currentHealth -= dmg
        critString = "Critical Hit!\n" if crit else ''
        self.text(f"{critString}{c.name} hits {target.name} with their {weap.name} for {dmg} damage!")
        if random.randint(0,3) == 0:
            self.text(self.retaliate(target,c))
        
        self.battleUpdate(c)

        
        if self.inBattle and c not in self.enemyList:
            self.battleTurn()
        return

    def retaliate(self,target,c):
        if target.currentHealth <= 0:
            return
        weap = target.equip
        stat = 0
        if weap.type == WeaponType.BLADE or weap.type == WeaponType.BLUNT:
            stat = target.strength #melee
        else:
            stat = target.agility #bows

        crit = True if random.randint(0,80) <= c.agilityStat else False
        dmg = max(0,int(weap.damageMult * random.randint(stat-2,stat+2)) - target.armorEquip.damageReduce)
        dmg *= 2 if crit else 1
        c.currentHealth -= dmg
        critString = "Critical Hit!\n" if crit else ''

        return f"{target.name} makes a retaliation attack! \n{critString}{target.name} hits {c.name} with their {weap.name} for {dmg} damage!"


    def battleAI(self,ai):
        # drink potion if low and they have one
        time.sleep(3)
        potions = [item.items['smallhealthpotion'],item.items['healthpotion'],item.items['largehealthpotion']]
        intersection = itemManager.intersection(potions,ai.items)
        if len(intersection)>0 and ai.currentHealth <=3:
            potion = intersection[0]
            ai.items.remove(potion)
            ai.currentHealth = min(ai.maxHealth, ai.currentHealth+ int(potion.effectParam))
            self.text(f"{ai.firstname} drinks a health potion to recover {int(potion.effectParam)} health")
            return
        
        # attack random player
        target = random.choice(list(self.playerDict.values()))
        self.attack(ai,target.name)


    def use(self,c,itemstr):
        itemstr = itemstr.lower()
        usable = None
        item = None
        for item in c.items:
            if itemstr == item.getSmushName():
                usable = item
        if item == None:
            return "You don't have an item with that name"
        return usable.effectFunc(usable,user=c)


    def levelup(self,c:Character,statStr):
        statStr = statStr.lower()
        xp_needed = c.level * c.XP_PER_LEVEL
        if c.xp < xp_needed:
            return f"you need another {xp_needed-c.xp} xp to level up."
        
        strStrs = ['strength','str','s']
        agiStrs = ['agility','agi','a']
        magStrs = ['magic','mag','m']

        if statStr in strStrs:
            c.strengthStat += 2
        elif statStr in agiStrs:
            c.agilityStat += 2
        elif statStr in magStrs:
            c.magicStat += 2
        else:
            return 'specify strength, agility, or magic to put focus points into!'

        c.healthStat += 2

        # set stats to max after leveling
        c.currentHealth =  c.maxHealth = c.healthStat
        c.tmpStrength = c.strength = c.strengthStat
        c.tmpMagic = c.magic = c.magicStat
        c.tmpAgility = c.agility = c.agilityStat

        c.xp -= xp_needed
        c.level += 1
        return f'{c.name} has reached level {c.level}!'


    def give(self,c,targetName,itemstr,amt='1'):
        if not amt.isdigit():
            return "bad input - /dm give [player] [item] (amount)"
        amt = int(amt)
        targetName = targetName.lower()
        itemstr = itemstr.lower()
        target = None
        party = [p for p in self.playerDict.values()]   

        for pc in self.currentLoc.getLocals(party):
            if targetName == pc.firstname.lower():
                target = pc
        if target == None:
            return f"Nobody around here is named {targetName}"

        delivery = quest.delivery(c,target)
        if delivery is not None:
            return delivery

        quest.foundItemCheck(c,target)
        finished = quest.finishedQuest(c,target)
        if finished is not None:
            return finished

        # give gold
        if itemstr.lower() == "gold":
            c.gold -= amt
            target.gold += amt
            return f"{c.name} gives {target.name} {amt} gold" 

        given = ''
        for a in range(0,amt):
            item = None
            for i in c.items:
                if itemstr == i.getSmushName():
                    item = i

            # give item is maybe a weapon
            if item is None:
                for w in c.weapons:
                    if itemstr == w.getSmushName():
                        item = w
            
            if item is None:
                return f"{c.name} doesn't have anything like this."
            
            # successfully given item 
            item.owner = target
            if type(item) is Weapon:
                target.weapons.append(item)
                c.weapons.remove(item)
            else:
                target.items.append(item)
                c.items.remove(item)

            more = ', ' if a < amt-1 else '' 
            given += item.name + more
        

        message = '' # add if pertaining to quest
        return f"{message}{c.name} gives {given} to {target.name}"
            

    def info(self,c,thingStr):
        thingStr = thingStr.lower()
        thing = None
        for i in c.items:
            if thingStr == i.getSmushName():
                thing = i
        
        for w in c.weapons:
            if thingStr == w.getSmushName():
                thing = w

        for s in c.spells:
            if thingStr == s.getSmushName():
                thing = s
            
        if self.currentLoc.hasShop:
            for widget in self.currentLoc.shop.shopInventory:
                if thingStr == widget.getSmushName():
                    thing = widget
                    self.text(f"{c.name} asks the shopkeep if they can look at the {thing.name}.")
                    break

        if self.inBattle:
            for bandit in self.enemyList:
                if thingStr == bandit.name.lower():
                    thing = bandit

        party = [p for p in self.playerDict.values()]   
        for p in self.currentLoc.getLocals(party):
            if thingStr == p.firstname.lower():
                thing = p

        local = ['here', 'area', 'surroundings','around']
        if thingStr == self.currentLoc.name.lower() or thingStr in local:
            thing = self.currentLoc

        if thing == None:
            return "Unrecognized name."

        if type(thing) is Weapon:
            return thing.description + "\n" + thing.getStatStr(c)
        
        if type(thing) is Character or type(thing) is Bandit:
            return thing.description + "\n" + thing.getStatStr()


        desc = thing.description
        if type(thing) is Location:
            desc = thing.description.format(self.currentLoc.name)
            self.currentLoc.passers = [random.choice(self.currentLoc.inhabitants) for _ in range(0,min(4,len(self.currentLoc.inhabitants)))]
            desc += "\n\nYou see some locals pass by:\n"
            for p in self.currentLoc.passers:
                desc += p.firstname + '\n'

        return desc


    def rest(self,c:Character):
        if self.currentLoc.type == locType.LocType.CITY or self.currentLoc.type == locType.LocType.VILLAGE:

            if self.bountyCheck(c): return

            cost = self.currentLoc.affluence
            if cost > c.gold:
                return f"you don't have enough gold for a stay at the inn ({cost} G)"
            
            #successful rest
            c.gold -= cost
            c.resetStats()
            return f"{c.name} pays the innkeep {cost} gold and awakens feeling refreshed"

        else:
            if c.hasItem('tent'):
                c.resetStats()
                c.removeItem('tent')
                return f"{c.name} sleeps in their tent and awakens feeling refreshed"
            return "You cannot rest out in the elements! Go to a village or town, or bring a tent next time."
        

    def bounty(self,c,payment='0'):
        if payment.lower() == 'all':
            payment = str(c.bounty) 
        if not payment.isdigit():
            return "bad input - /dm bounty (amount)"
        payment = int(payment)
        if payment <= c.gold and c.bounty > 0:
            c.gold -= min(payment,c.bounty)
            c.bounty = max(c.bounty-payment,0)
        else:
            self.text(f"{c.name} doesn't have enough for that")
        return f"{c.name} owes {c.bounty} gold"

    def tip(self,c):
        return random.choice(Tips)

    def changes(self,c):
        ver = changelog.VERSION
        resultstr = f"Version {ver}:\n\n"
        for change in changelog.changes[ver]:
            resultstr += "* " + change + "\n\n"
        
        return resultstr

    def logger(self,c,logstr):
        if self.inBattle:
            return "Please wait to log in or out until after the battle is finished!"
        logstr = logstr.lower()
        outstrs = ['out','off']
        instrs = ['in','on']
        #LOG OUT
        if logstr in outstrs:
            if c not in self.playerDict:
                return "You don't have a character!"
            self.loggedout[c] = self.playerDict[c]
            del self.playerDict[c]
            return "You successfully logged out. use '/dm log in' to log back in."
        #LOG IN
        elif logstr in instrs:
            if c not in self.loggedout:
                return "you don't have a character in the save file!"
            self.playerDict[c] = self.loggedout[c]
            del self.loggedout[c]
            return f"Welcome back {c}! or should I say {self.playerDict[c].name}?"


    def flee(self,c):
        if random.randint(0,40) <= c.agilityStat:
            prevLoc = c.knownLocs[-2]
            self.currentLoc = prevLoc
            self.enemyList = []
            self.inBattle = False
            self.turnOrder = []
            return f"The party successfully fled away from battle, back to {prevLoc.name}"
        self.text("Where do you think you're going?")
        self.battleTurn()
        return
        
    def talk(self,c,charName):
        char = None
        charName = charName.lower()
        for p in self.currentLoc.inhabitants:
            if charName == p.firstname.lower():
                char = p
        if char is None:
            return "No one by that name here."
        
        givesQuest = random.randint(0,6)
        questAlready = quest.givenQuest(c,char)
        delivery = quest.delivery(c,char)
        quest.foundItemCheck(c,char)
        finished = quest.finishedQuest(c,char)
        if finished is not None:
            return finished
        if delivery is not None:
            return delivery
        if questAlready is not None:
            return quest.getQuestDialogue(c,questAlready,'unfinished')
        elif givesQuest > 2 or char.questQueried:
            phrase = random.choice(storyText.phrases)
            char.questQueried = True
            return f"Greetings {c.name},\n{phrase}"
        else:
            newQuest = quest.generateQuest(c,self.currentLoc,char)
            char.givenQuest = True
            c.quests.append(newQuest)
            return quest.getQuestDialogue(c,newQuest,'new')
            


    def quests(self,c):
        questList = 'Quests:\n'
        for q in c.quests:
            questList += f"-  {q.summary()}\n"
        return questList

    def help(self,cmd=None):
        helpstr = ''

        if cmd == None:
            helpstr = 'DM Commands:\n\n'
            for cmd in sorted(self.commands):
                usage = self.commands[cmd]['usage']
                spacing = ' '*(15-len(cmd))
                helpstr += f'{cmd}:{spacing}{usage}\n'
        elif cmd in self.commands:
            helpstr = f"{cmd}\nUsage: /dm {cmd} {self.commands[cmd]['usage']}\n\n{self.commands[cmd]['desc']}"
        else:
            return f"{cmd} is not a command"
        return helpstr


    def __init__(self,id,bot,test):
        self.test_mode = test
        self.chat_id = id
        self.bot = bot
        self.world = None
        self.playerDict = {}
        self.currentLoc = None
        self.inBattle = False
        self.enemyList = []
        self.turnOrder = []
        self.loggedout = {}
        self.turn = 0
        self.battleTimeout = 0

        self.battleCommands = [
            'attack',
            'cast',
            'battlestats',
            'use',
            'equip',
            'inventory',
            'stats',
            'info',
            'flee',
            'pass',
        ]

        self.battleOnlyCommands = [
            'attack',
            'cast',
            'battlestats',
            'pass',
            'flee',
        ]

        self.turnEnders = [
            'cast',
            'attack',
            'pass',
            'flee',
        ]

        self.commands = {
            'help': {'fn': self.help, 'argc':[0,1], 'usage': '(command)', 'desc': 'Show the help menu or info about a specific command'},

            #'newworld': {'fn': self.newCampaign, 'argc': [0],  'usage': '/dm newworld', 'desc': 'Generate a new world'},

            'travel': {'fn': self.travel, 'argc': [1], 'usage': '[direction]', 'desc': 'Travel towards a location'},

            'fasttravel': {'fn': self.fasttravel, 'argc': [0,1], 'usage': '[location]', 'desc': 'Fast travel to a previously discovered place'},

            'directions': {'fn': self.directions, 'argc': [0], 'usage': '', 'desc': 'Get directions from a passerby'},

            'newcharacter': {'fn': self.newCharacter, 'argc': [1], 'usage': '[name] (race)', 'desc': 'Create a new character'},

            'stats': {'fn': self.charStats, 'argc': [0],  'usage': '', 'desc': 'See your character stats'},

            'inventory': {'fn': self.inventory, 'argc': [0],  'usage': '', 'desc': 'See your character\'s inventory'},

            'shop': {'fn': self.shop, 'argc': [0],  'usage': '', 'desc': 'See what the shop is selling, and how much they\'ll pay'},

            'buy': {'fn': self.buy, 'argc': [1],  'usage': '[item]', 'desc': 'Buy an item from the local shop'},

            'sell': {'fn': self.sell, 'argc': [1,2],  'usage': '[item] (amount)', 'desc': 'Sell an item to the local shop'},
            
            'bet': {'fn': self.bet, 'argc': [2],  'usage': '[amount] [heads/tails]', 'desc': 'Take a 50/50 gamble with a local ruffian'},

            'search': {'fn': self.search, 'argc': [0], 'usage': '', 'desc': 'Search the area for valuables'},

            'loot': {'fn': self.loot, 'argc': [1], 'usage': '[item]', 'desc': 'Loot items from a container or a body'},

            'info': {'fn': self.info, 'argc': [1], 'usage': '[thing]', 'desc': 'Get info about an item, weapon, or person'},

            'levelup': {'fn': self.levelup, 'argc': [1], 'usage': '[strength|agility|magic]', 'desc': 'Levels up your character, adding focus points to your strength, agility, or magic'},

            'give': {'fn': self.give, 'argc': [2,3], 'usage': '[player] [item] (amount)', 'desc': 'Give an item or weapon to another player'},

            'rest': {'fn': self.rest, 'argc': [0], 'usage': '', 'desc': 'While in a village or city, rest at an inn for a few gold to recover'},

            'bounty': {'fn': self.bounty, 'argc': [0,1], 'usage': '(payment)', 'desc': 'Check or pay bounty to be able to enter shops and inns'},

            'tip': {'fn': self.tip, 'argc': [0], 'usage': '', 'desc': 'Get a random tip about how to play'},

            'changelog': {'fn': self.changes, 'argc': [0], 'usage': '', 'desc': 'See what Ben has added to the wonderful world of Tegramalon'},

            'log': {'fn': self.logger, 'argc': [1], 'usage': '[out | in]', 'desc': 'Bring back or remove your character from play'},

            'flee': {'fn': self.flee, 'argc': [0], 'usage': '', 'desc': 'A chance to escape combat!'},

            'talk': {'fn': self.talk, 'argc': [1], 'usage': '[name]', 'desc': 'Speak with a local'},

            'quests' : {'fn': self.quests, 'argc': [0], 'usage': '', 'desc': 'See your active quests'},

            'challenge' : {'fn': self.challenge, 'argc': [1], 'usage': '[character]', 'desc': 'Start a battle with a non-combative NPC'},


            #commands used in battle
            'attack': {'fn': self.attack, 'argc': [1], 'usage': '[target]', 'desc': 'Attack target with equipped weapon'},

            'cast': {'fn': self.cast, 'argc': [2], 'usage': '[spell] [target]', 'desc': 'Cast specified spell at target'},

            'equip': {'fn': self.equip, 'argc': [1], 'usage': '[weapon/armor]', 'desc': 'Equip a weapon in your inventory'},

            'battlestats': {'fn': self.battleStats, 'argc': [0], 'usage': '', 'desc': 'See health and battle participants and turn-order'},

            'use': {'fn': self.use, 'argc': [1], 'usage': '[item]', 'desc': 'Use an item in your inventory'},

            'pass': {'fn': self.skipturn, 'argc': [0], 'usage': '', 'desc': 'Skip your turn during battle'},

        }
