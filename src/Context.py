
class Context():
    def __init__(self):
        self.args = []

    def tokenize(self, string):
        ends = [' ', ',', '"', '.', '!', '?']
        token_list = []
        token = ''
        for c in string:
            if c in ends:
                if token != '':
                    token_list.append(token)
                    token = ''
            else:
                token += c
        
        if token != '':
            token_list.append(token)
        return token_list


class Update():
    def __init__(self):
        self.message = Message()

class Message():
    def __init__(self):
        self.from_user = FromUser()

class FromUser():
    def __init__(self):
        self.first_name = "Ben"