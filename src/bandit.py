from math import degrees
import random
import armor
from character import Character
from weapon import weapons 

class Bandit(Character):
    def __init__(self, name,level):
        super().__init__(name)
        self.level = level
        self.fromQuest = False

        for _ in range(1,self.level):
            choice = random.randint(1,3)
            if choice == 1:
                self.strengthStat += 2
            if choice == 2:
                self.agilityStat += 2
            if choice == 3:
                self.magicStat += 2
        self.resetStats()
        self.maxHealth = int(((1 + ((2*(level-1)) / 10)) * 10) + random.randint(-3,2))


        getWeaponByLevel(self)

        getArmorByLevel(self)

        self.description = f"{self.name} is a level {self.level} bandit. They are using a {self.equip.name}"
        if self.armorEquip.name != 'Clothes':
            self.description += f" and {self.armorEquip.name}"
        self.statDesc = self.getStatStr()


def getWeaponByLevel(enemy):
    weaponsList = []
    for l in range(min(enemy.level,9),0,-1):
        weaponsList += weaponsByLevel[l]
    tmpWeap = weapons[random.choice(weaponsList)]
    enemy.equip = tmpWeap
    enemy.weapons.append(tmpWeap)


def getArmorByLevel(enemy):
    armorsList = []
    if enemy.level > 1 and random.randint(0,3) == 0:
        for l in range(min(enemy.level,7),1,-1):
            armorsList += armorsByLevel[l]
        tmpArmor = armor.armors[random.choice(armorsList)]
        enemy.armorEquip = tmpArmor
        enemy.items.append(tmpArmor)


weaponsByLevel = {
    1 : ['woodcudgel'],
    2 : ['bronzeknife'],
    3 : ['bronzeshortsword','bronzesword','ironknife','scrapbow'],
    4 : ['ironshortsword','steelknife','trainingbow','bronzewarhammer'],
    5 : ['ironsword','maplebow'],
    6 : ['steelsword','ironwarhammer'],
    7 : ['steellongsword','recurvebow'],
    8 : ['steelwarhammer','longbow'],
    9 : ['bladeofdwelako']
}

armorsByLevel = {
    1 : [],
    2 : ['leatherarmor'],
    3 : ['gambeson'],
    4 : ['bronzearmor'],
    5 : ['ironarmor'],
    6 : ['steelarmor'],
}