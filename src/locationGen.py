from enum import Enum
import string
import character
import nameGen
import locType
from shop import Shop
import itemManager
import weapon
import storyText
import random

class Location:
    def __init__(self, typeloc):
        self.type = typeloc
        affix = True if random.randint(1,11) > 1 else False     # 90% chance of affix
        self.name = string.capwords(nameGen.genLocName(random.randint(3,7),self.type, affix))
        self.smushName = nameGen.smushName(self.name)
        self.roads = [None,None,None,None,None,None,None,None,] # 8 directions
        self.hasShop = True if self.type == locType.LocType.CITY or self.type == locType.LocType.VILLAGE else False
        self.hasCorpse = False #set true after battle and false after looting
        self.corpses = [] #append opponent after their death
        self.looted = False #set true after looting
        self.containers = []
        self.beenSearched = False
        self.questItemCount = 0
        self.passers = []

        richSwitch = {
            locType.LocType.CAMP : 0,
            locType.LocType.CAVE : 0,
            locType.LocType.VILLAGE : random.randint(1,5),
            locType.LocType.CITY : random.randint(4,9),
        }
        self.affluence = richSwitch[self.type]

        popCount = {
            locType.LocType.CAMP : random.randint(1,2),
            locType.LocType.CAVE : random.randint(1,5),
            locType.LocType.VILLAGE : random.randint(8,20),
            locType.LocType.CITY : random.randint(18,75),
        }
        self.inhabitants = [character.genRandomNpc() for _ in range(0,popCount[self.type])]
        for villager in self.inhabitants:
            for _ in range(0,random.randint(0,3)):
                itemManager.randomItem(villager)
            villager.gold = random.randint(1,25)
            #give bronze knife if spawn with no weapons
            if len(villager.weapons) == 0:
                knife = weapon.new('bronzeknife')
                villager.weapons.append(knife)
            villager.equip = villager.getBestWeapon()

        for npc in self.inhabitants:
            npc.description = f"{npc.name} is a doofus npc that Ben hasn't coded a personality for"

        if self.hasShop:
            self.shop = Shop(location=self)
            self.inhabitants.append(self.shop.shopKeeper)

        if self.type == locType.LocType.CAMP:
            self.description = random.choice(storyText.campDescriptions)
        elif self.type == locType.LocType.CAVE:
            self.description = random.choice(storyText.caveDescriptions)
        elif self.type == locType.LocType.VILLAGE:
            self.description = storyText.villageDescriptions[self.affluence]
        elif self.type == locType.LocType.CITY:
            self.description = storyText.cityDescriptions[self.affluence]
        else:
            self.description = "ERROR ERROR YELL AT BEN"

            

    def getCorpses(self):
        if len(self.corpses) == 1:
            return self.corpses[0].name
        elif len(self.corpses) > 1:
            result = ''
            for i in range(0,len(self.corpses)-1):  #stop 1 before so it can be 1,2, and 3.
                result += f"{self.corpses[i].name}, "
            result += f"and {self.corpses[len(self.corpses)-1].name}"
            return result
        else:
            return "ERROR ERROR YELL AT BEN"

    def getLootables(self):
        lootables = []
        for npc in self.inhabitants:
            lootables.append(npc)
        for container in self.containers:
            lootables.append(container)
        return lootables

    def getLocals(self,party):
        return self.inhabitants + party
        
    def getNeighbors(self):
        return [self.roads[i].connections[1] for i in range(0,len(self.roads)) if self.roads[i] != None]

