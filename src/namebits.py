import locType

phonemes = {
    'consonent' : [
        'b',
        'c',
        'ch',
        'd',
        'f',
        'g',
        'h',
        'j',
        'k',
        'l',
        'm',
        'n',
        'p',
        'r',
        's',
        'sh',
        't',
        'th',
        'v',
        'w',
        'y',
        'z',
        'zh'
    ],

    'vowel' : [
       'a',
       'e',
       'i',
       'o',
       'u',
       'y',
       'ae',
       'ai',
       'ei',
       'oa',
       'ao',
    ]
}

weights = [
    [  # consonents
        10, # b
        10, # c
        5, # ch 
        12, # d
        10, # f
        10, # g
        8, # h
        8, # j 
        5, # k 
        15, # l
        10, # m
        12, # n
        10, # p
        15, # r
        15, # s
        6, # sh
        20, # t
        1, # th
        8, # v
        5, # w
        2, # y
        1, # z
        1, # zh
    ],

    [  # vowels
        20, # a
        30, # e
        15, # i 
        10, # o 
        5, # u
        1, # y
        1, # ae
        5, # ai
        3, # ei
        2, # oa
        3, # ao
    ]
]


locAffixes = {
    locType.LocType.CAVE: [
        ' depths',
        ' barrows',
        ' bowels',
        ' cavern',
        ' chambers',
        ' pit',
        ' cavern',
        ' hollow',
        ' grotto',
        '\'s depths',
        '\'s barrows',
        '\'s bowels',
        '\'s cavern',
        '\'s chambers',
        '\'s pit',
        '\'s cavern',
        '\'s hollow',
        '\'s grotto',
    ],

    locType.LocType.CAMP: [
        ' camp',
        ' clearing',
        '\'s camp',
        '\'s clearing'
    ],

    locType.LocType.VILLAGE: [
        'dorf',
        'ia',
        'wood',
        'vale',
        'ton',
        'land',
        'water',
        'dale',
        'glen',
        'burg',
        'mouth',
        'feld',
        'field',
        'ville',
        '\'s rest',
        '\'s vale',
        '\'s glen',
        '\'s valley',
        '\'s crossing',
        '\'s field',
    ],

    locType.LocType.CITY: [
        ' city',
        'polis',
        ' falls',
        ' hill',
        ' springs',
        ' rapids',
        ' wood',
        'ville',
        ' park',
        'ton',
        ' valley',
        'land',
        ' bend',
        'port',
        'ford',
        ' haven',
    ]
}