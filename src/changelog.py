VERSION = "1.3"

changes = {
    "1.3": [
        "Added quests. talk to some NPCs and some may have a task for you. Use the 'quests' command to see your active quests",
        "Implemented item rarity.",
        "Added 'challenge' command to start a battle with NPCs",
        "You can buy tents to rest in the field.",
        "Elves start off with a spellbook of zap",
        "/last - repeats your last action",
        "battle turns now say the turn-taker's HP",
        "Gold is a statistic now",
        "squashed le epic bugs 😎",
    ],

    "1.2.5" : [
        "Can't log in or out in battle",
        "Party wipe doesn't delete the world if someone is logged out",
        "You can now speak with NPCs with the 'talk' command!",
        "no more retaliation attacks after dying",
        "looting a body removes the need to search",
    ],

    "1.2" : [
        "Added Armor, equip to shave off some incoming damage!",
        "Added beans",
        "Made weapons progression more expensive, plus stat requirements",
        "1.5x increase of xp gained per kill",
        "Bandit spawn scales with average player level",
        "Bandits spawn with level appropriate weapons",
        "Slightly buffed bronze knife because doing 0 damage sucks",
        "Agility stat now affects turn order",
        "Reduced racial stat bonus from 14 -> 12",
        "Log in and Log out implemented (untested with other players)",
        "Battle timeout 120s -> 60s",
    ],

    "1.1" : [
        "Races! You can now be a Human, Dwarf, Elf, or Larchling for different starting stats!\nHuman: S: 10, M: 10, A: 10\nDwarf: S: 14, M: 8, A: 8\nElf: S: 8, M: 14, A: 8\nLarchling: S: 8, M: 8, A: 14",
        "Lowered XP needed to level up from 30 per level -> 10 per leveip",
        "XP is shared amongst friendlies",
        "You can now get info about an item in the shop",
        "added changelog (ooh meta)",
        "added 2 minute battle timeout, where if someone else does a battle command after 2 minutes, it will skip the tardy person's turn`"
        "you can do more during battle without it screaming at you to wait your turn",
        "battlestats lists player names to easily see who is a friendly",
        "help menu got rid of redundant '/dm [command]' part of usage to hopefully not wrap to next line and make messy",
        "various bug fixes",
    ]
}

Hopeful_changes = {
    "1.4" : [
        "Dungeon update baby! Delve into some caves for some more team-oriented combat, some good loot, and maybe even boss fights. Make sure you come prepared!",

    ],

    
}