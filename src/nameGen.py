import random
import namebits

class NPC_Name:
    firstName = ''
    lastName = ''
    name = ''
        

def nameGen(letters):
    name = ''
    sameCount = 0
    bit = 'vowel'

    for i in range(1,letters + 1):
        oldbit = bit
        if random.randint(0,1) != 0:
            if(bit == 'vowel'):
                bit = 'consonent'
            else:
                bit = 'vowel'
        if(oldbit == bit):
            sameCount += 1
            if(sameCount >= 1):
                if(bit == 'vowel'):
                    bit = 'consonent'
                else:
                    bit = 'vowel'
        
        weightSet = 1 if bit == 'vowel' else 0
        name += random.choices(namebits.phonemes[bit],namebits.weights[weightSet],k=1)[0]

    return name


def genNpcName(firstLetters, lastLetters):
    tmp = NPC_Name()
    tmp.firstName = nameGen(firstLetters)
    tmp.lastName = nameGen(lastLetters)
    tmp.name = tmp.firstName + ' ' + tmp.lastName
    return tmp.name.title()

def genLocName(letters, locType, affix):
    name = nameGen(letters)
    if(affix):
        name += random.choice(namebits.locAffixes[locType])
    return name

def smushName(string):
    string = string.replace("'","")
    string = string.replace(" ","")
    string = string.replace("-","")
    string = string.lower()
    return string
