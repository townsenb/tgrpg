import enum
class LocType(enum.Enum):
    CAVE = 0
    CAMP = 1
    VILLAGE = 2
    CITY = 3

def prettyLocType(typeLoc):
    if typeLoc == LocType.CAVE or typeLoc == 0: return 'cave'
    if typeLoc == LocType.CAMP or typeLoc == 1: return 'camp'
    if typeLoc == LocType.VILLAGE or typeLoc == 2: return 'village'
    if typeLoc == LocType.CITY or typeLoc == 3: return 'city'
    return str(typeLoc)