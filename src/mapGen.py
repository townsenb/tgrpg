import enum
from hashlib import new
import random
from direction import *
from nameGen import nameGen
from road import Road
from locationGen import Location
import locType

class Map:
    def __init__(self):
        self.startLocation = Location(locType.LocType.VILLAGE)
        self.currentLocation = self.startLocation
        self.totalSites = 1
        self.sitesCount = {locType.LocType.CITY:0,locType.LocType.VILLAGE:1,locType.LocType.CAMP:0,locType.LocType.CAVE:0}
        self.name = nameGen(random.randint(2,6)).title()

    def genMap(self,current,distFromCenter):
        if distFromCenter <= 0:
            return

        for d in range(0,8):
            global totalSites
            if(random.randint(0,8) <= 2):
                if current.roads[d] == None:
                    newLoc = Location(random.choices(list(locType.LocType),weights=[15,20,10,3],k=1)[0])
                    self.connectLocs(current,newLoc,random.randint(2,12),random.choice(list(Direction)))
                    self.totalSites += 1
                    self.sitesCount[newLoc.type] += 1
                    distFromCenter -= 1
                    self.genMap(newLoc,distFromCenter)
    
    def connectLocs(self,loc1,loc2,distance,dirFrom1):
        opDir = (dirFrom1.value + 4) % 8
        loc1.roads[dirFrom1.value]= Road(loc1,loc2,distance)
        loc2.roads[opDir]= Road(loc2,loc1,distance)

    def getSitesCount(self):
        return 'Cities: {}\nVillages: {}\nCamps: {}\nCaves: {}' \
                .format(self.sitesCount[locType.LocType.CITY],self.sitesCount[locType.LocType.VILLAGE], \
                        self.sitesCount[locType.LocType.CAMP],self.sitesCount[locType.LocType.CAVE])

def getConnections(loc):
    connections = []
    for i in range(0,8):
        if loc.roads[i] != None:
            string = intToDir(i) + " to " + str(loc.roads[i].connections[1].name)
            connections.append(string)
    return connections
