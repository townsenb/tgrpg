from enum import auto

import nameGen

class Effectables(auto):
    HEALTH = auto()
    STRENGTH = auto()
    MAGIC = auto()
    AGILITY = auto()

prettyEffectable = {
    Effectables.HEALTH : 'health',
    Effectables.STRENGTH : 'strength',
    Effectables.MAGIC : 'magic',
    Effectables.AGILITY : 'agility',
}


class Spell():
    def __init__(self,name,effect:Effectables,cost,minEffect,maxEffect,magicLevel):
        self.name = name
        self.effect = effect
        self.cost = cost
        self.min = minEffect
        self.max = maxEffect
        self.magicLevel = magicLevel
        self.description = self.getDescription()
        
    

    def getSmushName(self):
        return nameGen.smushName(self.name)

    def getDescription(self):
        effect = prettyEffectable[self.effect]
        negative = 'drains' if self.max <= 0 else 'restores'
        big = max(abs(self.min),abs(self.max))
        small = min(abs(self.min),abs(self.max))
        return f"{self.name}: {negative} {small} - {big} {effect}\nCosts {self.cost} magic"


spells = {
    'heal' : Spell('Heal',Effectables.HEALTH,cost=3,minEffect=2,maxEffect=5,magicLevel=10),
    'megaheal' : Spell('Mega Heal',Effectables.HEALTH,cost=7,minEffect=6,maxEffect=10,magicLevel=14),


    'zap' : Spell('Zap',Effectables.HEALTH,cost=1,minEffect=-3,maxEffect=-1,magicLevel=1),
    'fireball' : Spell('Fireball',Effectables.HEALTH,cost=3,minEffect=-7,maxEffect=-3,magicLevel=10),
    'cronch' : Spell('Cronch',Effectables.HEALTH,cost=5,minEffect=-12,maxEffect=-8,magicLevel=14),
    'nuke' : Spell('Nuke',Effectables.HEALTH,cost=9,minEffect=-30,maxEffect=-20,magicLevel=20),

    'weaken' : Spell('Weaken',Effectables.STRENGTH,cost=4,minEffect=-5,maxEffect=-3,magicLevel=10),
    'slow' : Spell('Slow',Effectables.AGILITY,cost=4,minEffect=-5,maxEffect=-3,magicLevel=10),

    'kill' : Spell('Kill',Effectables.HEALTH,cost=0,minEffect=-100,maxEffect=-100,magicLevel=1),
}
