import random
from enum import Enum
from item import Item
import itemManager

class ContainerType(Enum):
    SMALL = 0
    MEDIUM = 1
    LARGE = 3

class Container(Item):
    def __init__(self,type: ContainerType, name):
        self.name = name
        self.isCorpse = False
        self.items = [itemManager.randomItem(None) for _ in range(0,type.value)]
        self.gold = random.randint(1,(type.value+1)*20)

    def getItemList(self):
        result = ''
        for item in self.items:
            result += f"a {item.name}, "
        return result


containerNames = {
    ContainerType.SMALL: [
            'wallet',
            'coinpurse',
        ],
    ContainerType.MEDIUM: [
            'lockbox',
        ],
    ContainerType.LARGE: [
            'chest',
        ]  
}