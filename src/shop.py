from threading import local
from character import Character
import item
import itemManager
import nameGen
import random

class Shop():
    def __init__(self,location):
        self.location = location
        self.shopKeeper = Character(nameGen.genNpcName(random.randint(2,6),random.randint(3,7)).title())
        self.shopKeeper.description = f"{self.shopKeeper.name} is the shopkeeper for a small store in {location.name}"
        self.shopInventory = [itemManager.randomItem(self.shopKeeper) for _ in range(random.randint(5,max(location.affluence*3,5)))]
        for i in self.shopInventory:
            i.localPrice = int(i.value * (1 + location.affluence / 100)) + location.affluence

    def getShopInventoryWithPrice(self):        
        resultStrs = ''
        count = 1
        for item in self.shopInventory:
            resultStrs += str(count) + ": " + str(item.name + " - " + str(item.localPrice) + " G\n")
            count += 1
        return resultStrs

    def getShopInv(self):
        countDict = {i : self.shopInventory.count(i.name) for i in self.shopInventory}
        print(countDict)
        result = []
        for item in countDict:
            tmpstr = item.name
            if countDict[item] > 1:
                tmpstr += " {}x".format(countDict[item])
            tmpstr += f" -  {item.localPrice}"
            tmpstr += ' each' if countDict[item] > 1 else ''
            result.append(tmpstr)
        result = "\n".join(result)
        return result