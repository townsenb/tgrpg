from os import name
from item import Item
import enum
import random
import copy

class WeaponType(enum.Enum):
    BLUNT = 0
    BLADE = 1
    BOW = 2


class Weapon(Item):
    def __init__(self,name='',weapType=random.choice(list(WeaponType)),damageMult=0,baseValue=0,hands=0,minlev=0,rarity=0):
        super().__init__()
        self.name = name
        self.damageMult = damageMult
        self.value = baseValue
        self.type = weapType
        self.handed = hands # 1-dualwield, 1.5-onehanded w/ shield, 2-onlythatweap
        self.description = 'you know I always got that mf thang on me' + '\n' + str(baseValue) + " G"
        self.minLevel = minlev
    
    def __str__(self) -> str:
        return self.name

    def transferOwner(self,owner):
        if self.owner != None and self in owner.weapons: self.owner.weapons.remove(self)
        owner.weapons.append(self)
        self.owner = owner
    
    def getStatStr(self,player):
        statstr = ''
        if self.type is WeaponType.BOW:
            statstr += str(int(self.damageMult * (player.tmpAgility-2))) + ' - ' + str(int(self.damageMult * (player.tmpAgility+2)))
        else:
            statstr += str(int(self.damageMult * (player.tmpStrength-2))) + ' - ' + str(int(self.damageMult * (player.tmpStrength+2)))
        return statstr + ' dmg'


def new(weapStr):
    return copy.copy(weapons[weapStr])

weapons = {

    'woodcudgel' : Weapon('Wood Cudgel',WeaponType.BLUNT,damageMult=0.1,baseValue=1,hands=1,minlev=0,rarity=1),

    'bronzeknife' : Weapon('Bronze Knife',WeaponType.BLADE,damageMult=0.2,baseValue=6,hands=1,minlev=2,rarity=2),
    'bronzeshortsword' : Weapon('Bronze Shortsword',WeaponType.BLADE,damageMult=0.3,baseValue=50,hands=1,minlev=8,rarity=4),
    'bronzesword' : Weapon('Bronze Sword',WeaponType.BLADE,damageMult=0.4,baseValue=80,hands=1.5,minlev=10,rarity=8),
    'bronzewarhammer' : Weapon('Bronze Warhammer',WeaponType.BLUNT,damageMult=0.52,baseValue=140,hands=2,minlev=14,rarity=16),

    'ironknife' : Weapon('Iron Knife',WeaponType.BLADE,damageMult=0.25,baseValue=25,hands=1,minlev=4,rarity=3),
    'ironshortsword' : Weapon('Iron Shortsword',WeaponType.BLADE,damageMult=0.42,baseValue=100,hands=1,minlev=10,rarity=8),
    'ironsword' : Weapon('Iron Sword',WeaponType.BLADE,damageMult=0.5,baseValue=150,hands=1.5,minlev=12,rarity=14),
    'ironwarhammer' : Weapon('Iron Warhammer',WeaponType.BLUNT,damageMult=0.65,baseValue=250,hands=2,minlev=16,rarity=18),

    'steelknife' : Weapon('Steel Knife',WeaponType.BLADE,damageMult=0.35,baseValue=100,hands=1,minlev=10,rarity=4),
    'steelsword' : Weapon('Steel Sword',WeaponType.BLADE,damageMult=0.6,baseValue=300,hands=1.5,minlev=16,rarity=13),
    'steellongsword' : Weapon('Steel Longsword',WeaponType.BLADE,damageMult=0.8,baseValue=400,hands=2,minlev=18,rarity=18),
    'steelwarhammer' : Weapon('Steel Warhammer',WeaponType.BLUNT,damageMult=1,baseValue=600,hands=2,minlev=20,rarity=20),

    'bladeofdwelako' : Weapon('Blade of Dwelako',WeaponType.BLADE,damageMult=1.5,baseValue=2500,hands=2,minlev=24,rarity=25),

    'scrapbow' : Weapon('Scrap Bow',WeaponType.BOW,damageMult=0.3,baseValue=50,hands=2,minlev=10,rarity=3),
    'trainingbow' : Weapon('Training Bow',WeaponType.BOW,damageMult=0.4,baseValue=80,hands=2,minlev=12,rarity=5),
    'maplebow' : Weapon('Maple Bow',WeaponType.BOW,damageMult=0.55,baseValue=120,hands=2,minlev=14,rarity=8),
    'recurvebow' : Weapon('Recurve Bow',WeaponType.BOW,damageMult=.8,baseValue=320,hands=2,minlev=18,rarity=12),
    'longbow' : Weapon('Long Bow',WeaponType.BOW,damageMult=1,baseValue=500,hands=2,minlev=20,rarity=16),

}