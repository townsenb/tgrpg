import random
import enum
import armor
import nameGen
import locType
import random


class Character:
    XP_PER_LEVEL = 10

    def __init__(self,name):
        self.name = name
        self.player = None
        self.firstname = self.name.split()[0]
        self.surname = self.name.split()[1] if ' ' in self.name else ''
        self.smushName = ''
        self.level = 1
        self.xp = 0
        
        # character stat
        self.healthStat = 10
        self.strengthStat = 10
        self.magicStat = 10
        self.agilityStat = 10

        # variable max from buffs / debuffs
        self.maxHealth = self.healthStat
        self.strength = self.strengthStat
        self.magic = self.magicStat
        self.agility = self.agilityStat

        # current values
        self.currentHealth = self.maxHealth
        self.tmpStrength = self.strength
        self.tmpMagic = self.magic
        self.tmpAgility = self.agility

        
        self.weapons = []
        self.equip = None
        self.armorEquip = armor.Armor("Clothes",'0',0,0,"oopsie poopsie")
        self.spells = []

        self.gold = 10
        self.items = []

        self.bounty = 0

        self.knownLocs = []
        self.oldLoc = None

        self.description = ''
        self.race = 'human'

        self.quests = []
        self.questQueried = False

        self.pickpocketed = False
        
        self.statDesc = self.getStatStr()

        self.lastCmd = None
        self.lastArgs = []

    def hasItem(self,itemName):
        itemName = nameGen.smushName(itemName)
        for item in self.items:
            if item.getSmushName() == itemName:
                return True
        return False

    def removeItem(self,itemName):
        itemName = nameGen.smushName(itemName)
        for item in self.items:
            if item.getSmushName() == itemName:
                self.items.remove(item)
                return True
        return False

    def getBestWeapon(self):
        best = None
        bestDmg = 0
        for w in self.weapons:
            if w.damageMult > bestDmg:
                bestDmg = w.damageMult
                best = w
        return best


    def getItemList(self):
        result = ''
        for item in self.items:
            result += f"a {item.name}, "
        return result

    def getItemStrCondensed(self):
        countDict = {i.name : self.items.count(i) for i in self.items}
        result = []
        for item in countDict:
            tmpstr = ''
            if self.armorEquip != None:
                print(f"armor: {self.armorEquip.name}")
                if item == self.armorEquip.name:
                    tmpstr += '* ' 
            tmpstr += item
            if countDict[item] > 1:
                tmpstr += " {}x".format(countDict[item])
            result.append(tmpstr)
        return result

    def getSellables(self):
        return self.weapons + self.items

    def getKnownLocsWithNums(self):
        tmpstr = ''
        count = 1
        for loc in self.knownLocs:
            tmpstr += f"{count}: {loc.name} ({locType.prettyLocType(loc.type)})\n"
            count += 1
        return tmpstr

    def getSmushName(self):
        self.smushName = nameGen.smushName(self.firstname)
        return self.smushName

    def getStatStr(self):
        self.statDesc = f"Health: {self.currentHealth}/{self.maxHealth}\nStrength: {self.tmpStrength}/{self.strength}\nMagic: {self.tmpMagic}/{self.magic}\nAgility: {self.tmpAgility}/{self.agility}"
        return self.statDesc

    def resetStats(self):
        self.tmpAgility = self.agility = self.agilityStat
        self.tmpMagic = self.magic = self.magicStat
        self.tmpStrength = self.strength = self.strengthStat
        print(f"{self.tmpStrength} - {self.strength} - {self.strengthStat}")
        self.currentHealth = self.maxHealth = self.healthStat


    def npcLevelUp(self):
        for _ in range(1,self.level):
            choice = random.randint(1,3)
            if choice == 1:
                self.strengthStat += 2
            if choice == 2:
                self.agilityStat += 2
            if choice == 3:
                self.magicStat += 2
        self.resetStats()
        self.maxHealth = int(((1 + ((2*(self.level-1)) / 10)) * 10) + random.randint(-3,2))

def genRandomNpc():
    return Character(nameGen.genNpcName(random.randint(2,6),random.randint(2,8)))

races = ['human','dwarf','elf','larchling', "orc", "goblin"]

class Race(enum.Enum):
    HUMAN = 0
    DWARF = 1
    ELF = 2
    LARCHLING = 3
    ORC = 4
    GOBLIN = 5