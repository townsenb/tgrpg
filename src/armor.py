from item import Item

class Armor(Item):
    def __init__(self, name="", parameter=None, minLevel=0, baseValue=0, rarity=0, desc=''):
        super().__init__(name=name, function=None, parameter=parameter, baseValue=baseValue, desc=desc)
        self.damageReduce = int(parameter)
        self.minLevel = minLevel
        self.rarity = rarity
        self.description += f'\n{self.damageReduce} DR'

    

armors = {
    'leatherarmor' : Armor("Leather Armor",'1',10,50,1,"A crudely built chestpiece made from cured hide"),
    'gambeson' : Armor("Gambeson",'2',12,100,5,"A vest made of quilted wool. Suriprisingly strong for its material"),
    'bronzearmor' : Armor("Bronze Armor",'4',14,200,12,"A chesplate made from hammered bronze"),
    'ironarmor' : Armor("Iron Armor",'5',16,400,18,"A heavy "),
    'steelarmor' : Armor("Steel Armor",'6',20,800,22,"A beautifully crafted set of armor, adorned with gold accents."),
    'dwelakanchainmail' : Armor("Dwelakan Chainmail",'15',26,5000,99,"The armor that the mythical hero Dwelako was said to wear to battle."),
}