import enum

class Direction(enum.Enum):
    NORTH = 0
    NORTHEAST = 1
    EAST = 2
    SOUTHEAST = 3
    SOUTH = 4
    SOUTHWEST = 5
    WEST = 6
    NORTHWEST = 7

def intToDir(i):
    switch = {
        0: 'North',
        1: 'North-East',
        2: 'East',
        3: 'South-East',
        4: 'South',
        5: 'South-West',
        6: 'West',
        7: 'North-West',
    }
    return switch[i]

def parseDirection(dirstr):
    dirOptions = {
        0 : ['n','north'],
        1 : ['ne','northeast','north-east'],
        2 : ['e','east'],
        3 : ['se','south-east','southeast'],
        4 : ['s','south'],
        5 : ['sw','southwest','south-west'],
        6 : ['w','west'],
        7 : ['nw','northwest','north-west'],
    }

    for i,s in dirOptions.items():
        if dirstr in s:
            return i
    return -1
