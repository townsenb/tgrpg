import enum
import spell
import nameGen

class Condition(enum.Enum):
    OLD = 1
    USED = 5
    NEW = 10


class Item:
    def __init__(self, name="",function=None,parameter=None,baseValue=0,rarity=0,desc=''):
        self.name = name
        self.smushName = ''
        self.owner = None
        self.value = baseValue
        self.localPrice = 0
        self.rarity = rarity
        self.description = desc + '\n' + str(baseValue) + " G"
        self.effectFunc = function
        self.effectParam = parameter
    
    def transferOwner(self,owner):
        if self.owner != None: self.owner.items.remove(self)
        owner.items.append(self)
        self.owner = owner

    def getSmushName(self):
        self.smushName = nameGen.smushName(self.name)
        return self.smushName

    
    ''' EFFECT FUNCTIONS '''

    def heal(self, user):
        health = int(self.effectParam)
        user.currentHealth = min(user.currentHealth+health,user.maxHealth)
        user.items.remove(self)
        return f"{user.name} drinks the potion and recovers {health} health!"
        
    def strengthen(self,user):
        amt = int(self.effectParam)
        # allow up to max strength + potion amount
        user.tmpStrength = min(user.tmpStrength+amt,user.strength+amt)
        user.items.remove(self)
        return f"{user.name} drinks the potion and gains {amt} strength"

    def agilify(self,user):
        amt = int(self.effectParam)
        # allow up to max agility + potion amount
        user.tmpAgility = min(user.tmpAgility+amt,user.agility+amt)
        user.items.remove(self)
        return f"{user.name} drinks the potion and gains {amt} agility"

    def magify(self,user):
        amt = int(self.effectParam)
        # allow up to max strength + potion amount
        user.tmpMagic = min(user.tmpMagic+amt,user.magic+amt)
        user.items.remove(self)
        return f"{user.name} drinks the potion and gains {amt} magic"

    def learnSpell(self,user):
        learned = spell.spells[self.effectParam]
        if user.magic < learned.magicLevel:
            return f"you don't got the facilities for that, big man. (need magic level of {learned.magicLevel} or higher)"
        user.spells.append(learned)
        user.items.remove(self)
        return f"{user.name} flips through the pages, chanting uncomfortably, and learns the {learned.name} spell!"

    def trinket(self,user):
        return f"{user.name} tosses the {self.name} around in their hands. nothing happens."

    def beans(self,user):
        return f"{user.name} squishes the {self.name} in their fist. It's gushy."
    
    def tent(self,user):
        return f"use the 'rest' command to use your tent in the wilderness!"



items = {
    #POTIONS
    'smallhealthpotion' : Item("Small Health Potion",Item.heal,"5",baseValue=20,rarity=3,desc='Heals 5 HP worth of damage'),
    'healthpotion' : Item("Health Potion",Item.heal,"10",baseValue=50,rarity=5,desc='Heals 10 HP worth of damage'),
    'largehealthpotion' : Item("Large Health Potion",Item.heal,"20",baseValue=100,rarity=15,desc='Heals 20 HP worth of damage'),

    'strengthpotion' : Item("Strength Potion",Item.strengthen,"5",baseValue=75,rarity=5,desc="Replenish or increase user's Strength by 5 points"),
    'agilitypotion' : Item("Agility Potion",Item.agilify,"5",baseValue=60,rarity=5,desc="Replenish or increase user's Agility by 5 points"),
    'magicpotion' : Item("Magic Potion",Item.magify,"5",baseValue=40,rarity=5,desc="Replenish or increase user's Magic by 5 points"),


    #SPELLBOOKS
    'spellbookofheal' : Item("Spellbook of Heal",Item.learnSpell,"heal",baseValue=25,rarity=5,desc="A dusty tome used to learn the Heal spell"),
    'spellbookofmegaheal' : Item("Spellbook of Mega Heal",Item.learnSpell,"megaheal",baseValue=140,rarity=18,desc="A dusty tome used to learn the Heal spell"),


    'spellbookofzap' : Item("Spellbook of Zap",Item.learnSpell,"zap",baseValue=30,rarity=2,desc="A small booklet used to learn the Zap spell"),
    'spellbookoffirebll' : Item("Spellbook of Fireball",Item.learnSpell,"fireball",baseValue=90,rarity=7,desc="A book used to learn the Fireball spell"),
    'spellbookofcronch' : Item("Spellbook of Cronch",Item.learnSpell,"cronch",baseValue=150,rarity=15,desc="A rather large book with a cover depicting violent acts, used to learn the Cronch spell"),
    'spellbookofnuke' : Item("Spellbook of Nuke",Item.learnSpell,"nuke",baseValue=600,rarity=20,desc="A hefty volume bound with slate and sinew used to learn the Nuke spell"),

    'spellbookofweaken' : Item("Spellbook of Weaken",Item.learnSpell,"weaken",baseValue=55,rarity=6,desc="A leather-bound book used to learn the Weaken spell"),
    'spellbookofslow' : Item("Spellbook of Slow",Item.learnSpell,"slow",baseValue=45,rarity=6,desc="A leather-bound book used to learn the Slow spell"),


    #TRINKETS
    'amulet' : Item("Amulet",Item.trinket,"",baseValue=15,rarity=7,desc="A small amulet, may be worth some gold!"),
    'brassring' : Item("Brass Ring",Item.trinket,"",baseValue=3,rarity=2,desc="A crude ring of brass, possibly a family heirloom."),
    'silverring' : Item("Silver Ring",Item.trinket,"",baseValue=15,rarity=6,desc="A silver ring engraved with a light weaving pattern"),
    'goldring' : Item("Gold Ring",Item.trinket,"",baseValue=40,rarity=14,desc="A gold ring with beautiful inlaid rubies"),
    'rawgem' : Item("Raw Gem",Item.trinket,"",baseValue=15,rarity=8,desc="A raw gem whose scuffed faces have seen better days"),
    'cutgem' : Item("Cut Gem",Item.trinket,"",baseValue=50,rarity=18,desc="This cut gem has a decent heft in your hand. Looks prepared and polished by a professional."),

    'beans' : Item("Beans",Item.beans,"",baseValue=1,rarity=5,desc="Just a raw handful of wet beans."),

    'tent' : Item("Tent",Item.tent,"",baseValue=60,rarity=2,desc="A simple tent, used to sleep in whilst away from civilization."),
}
