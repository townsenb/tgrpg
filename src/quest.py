from enum import Enum
from bandit import Bandit
from character import Character
import item
import random
from locType import LocType
from nameGen import nameGen
import storyText


class QuestType(Enum):
    KILL = 0
    FIND = 1
    DELIVER = 2


class Quest():
    def __init__(self, giverName='', giverLoc=None, item=None ,targetName='',targetLoc=None, reward=0, questType=None):
        self.giver = giverName
        self.giverLoc = giverLoc
        self.item = item
        self.target = targetName
        self.targetLoc = targetLoc
        self.reward = reward
        self.type = questType
        self.isActive = True

    def summary(self):
        if self.type == QuestType.KILL:
            return f"Kill {self.target} at {self.targetLoc.name} for {self.giver} from {self.giverLoc.name}"
        elif self.type == QuestType.FIND:
            return f"Find {self.item.name} at {self.targetLoc.name} for {self.giver} from {self.giverLoc.name}"
        elif self.type == QuestType.DELIVER:
            return f"Deliver {self.item.name} to {self.target} at {self.targetLoc.name} for {self.giver} from {self.giverLoc.name}"


    def itemUse(self,user):
        return "This is for a quest, better not tamper with it."

def givenQuest(player,char):
    name = char.name
    for q in player.quests:
        if q.giver == name:
            return q
    return None

def finishedQuest(player,char):
    q = givenQuest(player,char)
    if q is not None:
        if not q.isActive:
            player.quests.remove(q)
            player.gold += q.reward
            player.xp += 10
            return getQuestDialogue(player,q,"done")
    return None

def foundItemCheck(player,char):
    q = givenQuest(player,char)
    if q is not None:
        if q.type == QuestType.FIND:
            q.isActive = False
            player.items.remove(q.item)
            return True
    return False

def delivery(player,char):
    name = char.name
    for q in player.quests:
        if q.target == name:
            if q.type == QuestType.DELIVER:
                if q.item in player.items:
                    player.items.remove(q.item)
                    q.isActive = False
                    return random.choice(storyText.deliveryMade).format(player.name, q.item.name, q.giver)
    return None

def killedTarget(allies,target):
    for a in allies:
        for q in a.quests:
            if q.target == target.name and q.type == QuestType.KILL:
                q.isActive = False
                return True

def getQuestLoc(currentLoc,questType):
    distance = random.randint(1,3)
    alreadyVisited = []
    tmpLoc = currentLoc
    for i in range(distance):
        nextLoc = random.choice(tmpLoc.getNeighbors())
        if nextLoc not in alreadyVisited:
            if questType != QuestType.KILL or (questType == QuestType.KILL and (nextLoc.type == LocType.CAMP or nextLoc.type == LocType.CAVE)):            
                alreadyVisited.append(nextLoc)
                tmpLoc = nextLoc

    if len(alreadyVisited) == 0:
        for n in currentLoc.getNeighbors():
            if questType != QuestType.KILL and (n.type == LocType.CAMP or n.type == LocType.CAVE):
                return n
        return nextLoc
    return alreadyVisited[-1]


def generateQuest(character, currentLoc, giver):
        questType = random.choice(list(QuestType))
        questTarget = Character("ERROR")
        questItem = None
        questLoc = getQuestLoc(currentLoc,questType)
        reward = random.randint(50,100)
        if questType == QuestType.KILL:
            reward *= 3
            # Create bandit, add to location, TRIGGER BATTLE ON TRAVEL UGH or make able to attack anyone?
            questTarget = Bandit(nameGen(random.randint(3,6)).title(),character.level+1)
            questTarget.fromQuest = True
            questLoc.inhabitants.append(questTarget)
        elif questType == QuestType.FIND:
            questLoc.questItemCount += 1
            questItem = questItems[questType][random.choice(list(questItems[questType].keys()))]
        elif questType == QuestType.DELIVER:
            questItem = questItems[questType][random.choice(list(questItems[questType].keys()))]
            questTarget = random.choice(questLoc.inhabitants)
            pass

        newQuest = Quest(giver.name,currentLoc,questItem,questTarget.name,questLoc,reward,questType)
        return newQuest


def getQuestDialogue(character,quest,stage=''):
    if stage == 'new':
        if quest.type == QuestType.KILL:
            return random.choice(storyText.questGiven[QuestType.KILL]).format(character.name, quest.targetLoc.name, quest.target, quest.reward)
        elif quest.type == QuestType.FIND:
            return random.choice(storyText.questGiven[QuestType.FIND]).format(character.name, quest.item.name, quest.targetLoc.name, quest.reward)
        elif quest.type == QuestType.DELIVER:
            character.items.append(quest.item)
            return random.choice(storyText.questGiven[QuestType.DELIVER]).format(character.name, quest.item.name, quest.target, quest.targetLoc.name, quest.reward)

    if stage == 'unfinished':
        if quest.type == QuestType.KILL:
            return random.choice(storyText.questNotDone[QuestType.KILL]).format(character.name,quest.target)
        elif quest.type == QuestType.FIND:
            return random.choice(storyText.questNotDone[QuestType.FIND]).format(character.name,quest.item.name)
        elif quest.type == QuestType.DELIVER:
            return random.choice(storyText.questNotDone[QuestType.DELIVER]).format(character.name,quest.item.name,quest.target)

    if stage == 'done':
        if quest.type == QuestType.KILL:
            return random.choice(storyText.questFinished[QuestType.KILL]).format(character.name,quest.target,quest.reward)
        elif quest.type == QuestType.FIND:
            return random.choice(storyText.questFinished[QuestType.FIND]).format(character.name,quest.item.name,quest.reward)
        elif quest.type == QuestType.DELIVER:
            return random.choice(storyText.questFinished[QuestType.DELIVER]).format(character.name,quest.item.name,quest.target,quest.reward)

questItems = {

    QuestType.FIND : {
        "grandmasring" : item.Item(name="Grandma's Ring",function=Quest.itemUse,baseValue=25,desc="A precious family heirloom."),
        "housekey" : item.Item(name="House Key",function=Quest.itemUse,baseValue=3,desc="A sturdy iron key."),
        "bagofnails" : item.Item(name="Bag of Nails",function=Quest.itemUse,baseValue=5,desc="A bag of nails."),
        "luckyspoon" : item.Item(name="Lucky Spoon",function=Quest.itemUse,baseValue=2,desc="Looks like a pretty normal spoon."),
        "petgerbil" : item.Item(name="Pet Gerbil",function=Quest.itemUse,baseValue=4,desc="A cute little gerbil named bingles."),
    },

    QuestType.DELIVER : {
        "urgentletter" : item.Item(name="Urgent Letter",function=Quest.itemUse,baseValue=1,desc="A wax sealed letter. seems important!"),
        "bottleofwhiskey" : item.Item(name="Bottle of Whiskey",function=Quest.itemUse,baseValue=15,desc="A glass bottle full of potent brown liquid."),
        "wintercoat" : item.Item(name="Winter Coat",function=Quest.itemUse,baseValue=20,desc="A warm coat for the winter."),
        "bronzeingot" : item.Item(name="Bronze Ingot",function=Quest.itemUse,baseValue=10,desc="A heavy bronze ingot used in smithing."),
        "seedpacket" : item.Item(name="Seed Packet",function=Quest.itemUse,baseValue=2,desc="A pouch of seeds of mixed vegetables."),
        "rollofpaper" : item.Item(name="Roll of Paper",function=Quest.itemUse,baseValue=1,desc="Tree pulp pressed into a lattice. Useful for writing."),
    }
}

