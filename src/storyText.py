from container import ContainerType
import quest

'''
    GENERAL DIALOGUE
'''
phrases = [
    "wonderful weather we're having!",
    "spare a coin, would ye please? I've got no bread for me family!",
    "how are ye today?",
    "fancy seeing you here!",
    "watch your back, there's shady folk in these woods",
    "got any stories from your adventures?",
    "I heard from the boys down at the tavern that the kings daughter is downright ugly!",
]

#.format(name,locType,locName,goldAmt,weapon)


newlyMadeCharacter = [
    'Greetings, {}.\nYou find yourself poor and destitute on the side of a road near the {} known as {} with naught but {} gold and an old {} for protection. ',
    'A sudden noise wakes you from a deep sleep. You, {}, recognize your whereabouts as the {} of {}. You check your pockets to find only {} gold and an old {} to defend yourself.'
]
#.format(name)
lootArea = '{} searches nearby belongings, and finds: \n\n{}'
#.format()
lootAreaIllegalInit = 'Taking from this area is considered stealing! if you are caught you will acquire a bounty. do you still want to?' 
#.format(name)
lootAreaIllegalSuccess = '{} successfully sneaks around any prying eyes, and makes off with: \n\n{}'
#.format(playerName, randomNPCname)
lootAreaIllegalFail = 'Oh no! {} bumped into a comically large vase, calling the attention of nearby {}, who will report this to the authorities!'

'''
    QUEST DIALOGUE
'''
questGiven = {
    quest.QuestType.KILL :
        # .format(charName, locationName, banditName, reward)
        [
            "Hey {}, There's a bandit in {} named {} who robbed me a few weeks back. Could you take care of them for me? I'll pay you {} gold",
            "Ever tried mercenary work, {}? There's a ruffian over at {} named {} who cheats at dice. I don't like cheaters, catch my drift? If they disappear I'll pay you {} gold.",
        ],
    quest.QuestType.FIND :
         # .format(charName, itemName, locationName, reward)
        [
            "Glad I ran into you, {}! I was meaning to ask a favor. I think I dropped my {} somewhere around {}. Would you mind looking for it? I'll give you {} gold if you can bring it back.",  
            "Oh {}! Would you mind helping me find my {}? I left it over by {} while travelling. Bring it back for me, would ya? I'll pay you {} gold.",  
        ],
    quest.QuestType.DELIVER :
        # .format(charName, itemName, targetName, locationName, reward)
        [
            "Good day {}, I've got a {} that needs to be delivered to {} in {}. If you're up for it, there's {} gold in it for ya.",
            "Hello {}! Could you deliver my {} to {} in {}? I'll pay you {} gold if you can do it. Thanks!",
        ]
}

questNotDone = {
    quest.QuestType.KILL : [
        # .format(charName, banditName)
        "Greetings {}, did you take care of {} yet?",
        "Hey {}, is {} dead yet? no? get to it!",
    ],
    quest.QuestType.FIND : [
        # .format(charName, itemName)
        "Hey {}, did you find my {} yet?",
        "Hey {}, I'm still looking for my {}. I'll be waiting here.",
    ],
    quest.QuestType.DELIVER : [
        # .format(charName, itemName, targetName)
        "Hey {}, did you deliver my {} to {} yet?",
        "Hey {}, I'm still waiting for my {} to be delivered to {}.",
    ]
}

questFinished = {
    quest.QuestType.KILL : [
        # .format(charName, banditName, reward)
        "{}, I heard you took out {}! here's that {} gold I promised.",
        "Hello {}! Rumor has it that {} is dead, well done! {} gold, as per the agreement.",
    ],
    quest.QuestType.FIND : [
        # .format(charName, itemName, reward)
        "{}, I heard you found my {}! Here's that {} gold I promised.",
        "Hello {}! Oh I see you found my {}! Here's the {} gold I owe you",
    ],
    quest.QuestType.DELIVER : [
        # .format(charName, itemName, targetName, reward)
        "{}, I heard you delivered the {} to {}! Here's that {} gold",
        "Greetings {}, Did you deliver that {} to {}? great! Here's {} gold for your efforts.",
    ]
}


foundWhere = [
    "nestled between some rocks",
    "sitting in the dirt",
    "in an old burlap sack",
    "being pecked at by some birds",
]

deliveryMade = [
    "Oh {}, is that {} for me? Give my thanks to {}.",
    "You {}? Got that {} {} told you about? Perfect, I've been waiting for this. Thanks.",
]


'''
    BATTLE DIALOGUE
'''
#.format(banditGroup)
initiate = 'Bandit Attack! \nEnemies: \n{}'
#.format(sender,target,weapon,damage)
attack = '{} attacks {} with their {}.\nDeals {} damage.'
#.format(sender,target,weapon,damage)
responseAttack = '{} attacks {} back with their {} and deals {} damage.'
#.format(sender,target,spell,effect,number,stat)
cast = '{} casts {} on {}, who {} {} {}'
#.format(playername, enemyname, lootArray)
lootAfterBattle = '{} searches the corpse of {} and finds: \n\n{}'

'''
    SHOP DIALOGUE
'''
#.format(npcName,playerName,inventoryWithCosts)
shop = 'A shopkeep named {} smiles at you. \n"Greetings, {}! Please have a look around my shop!"\n\nInventory:\n{}'
#.format(npc,name,itemName,cost)
bought = '{}: "Thank you for your business {}! That {} will cost you {} gold."'
#.format(npc,playerName)
notEnoughGold = '{}: "I think you might need a bigger wallet for that, my dear {}"'
#format(npc,article,itemname,plural,playername,gold)
sold = '{}: "I\'m sure I could find a use for {} {}{}. Here {}, take {} gold."'


'''
    AREA DESCRIPTIONS
'''
#affluence 1-5
villageDescriptions = {
    #.format(name)
    1: 'The small village of {} is a dump. Manure pervades every pocket of air indoors and out. Everyone who isn\'t a farmhand is begging for coin in the muddy road',
    2: 'Not much is going on in {}. The few passersby you see have a grumpy and tired look about them.',
    3: 'As you walk through the village of {}, you pass by various half-timbered houses and barns. The sounds of healthy livestock can be heard in the distance',
    4: 'A few village children are playing in the street, laughing. The houses in {} are built well and sturdy. Locals seem happy.',
    5: 'These villagers of {} have found a slice of luxury out here in the country. Beautiful views of the mountains are the backdrop to this idyllic town.',
}

#affluence 4-9
cityDescriptions = {
    #.format(name)
    4: 'It is quite obvious the city of {} has been ravaged by war or famine, maybe both. Emaciated children on the streets cling to your legs, begging for you to take them away.',
    5: 'Poverty has struck in the city of {}. There are beggars on most streets, but the people seem strong and resiliant. They will make it through.',
    6: '{} is full of merchants making a quick coin on the cheap prices and somewhat desperate populace. ',
    7: '{} seems a comfortable city indeed. Everyone looks well fed and employed. What more could one ask for?',
    8: 'The city of {} stands out of the rugged landscape as a haven to travelers. ',
    9: 'The grand city of {} is truly something to behold! Glorious marble buildings are not only dwellings and businesses, but works of art fit for the gods! It seems like everybody is smiling in this shining jewel of a city.',
}

#random
caveDescriptions = [
    #.format(name)
    '{} appears to just be a naturally formed slab of stone jutting out from the hillside. You can tell that a few travelers (and even a few vagrants) have spent nights here for shelter.',
    '{} seems to be a rather significant man-made hole dug out of ground. Perhaps it was a construction project? Perhaps a mass grave? Who\'s to say.',
    'The tunnels of {} are quite expansive! you can hear a deep echo as you step into the cave mouth. A slight trickle of water can be heard in the distance.',
]

#random
campDescriptions = [
    #.format(name)
    '{} is a small break in the forest a few hundred feet from the road. A fire pit in the clearing gave travelers warmth during a blistery night.',
    '{} is full broken down caravans and traveling gear strewn about.',
    '{} seems like an open area to keep a watchful eye as your companions rest.',
]


'''
    SEARCH CUES
'''
villageSearchables = {
    'fail' :[       #.format(charName)
        '{} searches around the village a bit but doesn\'t find anything of interest',
    ],
    'succeed' :{    #.format(charName,containerName)
        ContainerType.SMALL: [
            '{} looks along the ground and finds a {} sitting in the dirt!',
        ],
        ContainerType.MEDIUM: [
            'After making their way behind an abandoned woodshed, {} sees a {} nestled in the dirt!',       
        ],
        ContainerType.LARGE: [
            '{} sees an unusual mound in a far off field and decides to dig it up. It\'s a {}!',
        ]  
    }
}

citySearchables = {
    'fail' :[       #.format(charName)
        '{} pokes around the city center, but can\'t see any good leads',
    ],
    'succeed' :{    #.format(charName,containerName)
        ContainerType.SMALL: [
            '{} looks down at the street and sees a {} stuck between two cobblestones!',
        ],
        ContainerType.MEDIUM: [
            'Whilst walking along, {} finds a {} on the side of the road!',      
        ],
        ContainerType.LARGE: [
            '{} discovers a {} in the alley off a busy street!',
        ]  
    }
}


caveSearchables = {
    'fail' :[       #.format(charName)
        '{} searches through the cave but it is too dark to see anything.',
    ],
    'succeed' :{    #.format(charName,containerName)
        ContainerType.SMALL: [
            '{} finds a {} near the mouth of the cave!',
        ],
        ContainerType.MEDIUM: [
            '{} delves deeper into the cave and discovers a {} tucked against the rockface!',        
        ],
        ContainerType.LARGE: [
            '{} explores the cave and finds a {} in a small cavern!',
        ]  
    }
}

campSearchables = {
    'fail' :[       #.format(charName)
        '{} scans the area but whoever camped here didn\'t leave much.',
    ],
    'succeed' :{    #.format(charName,containerName)
        ContainerType.SMALL: [
            '{} spots a {} tucked inside a supply bag!',
        ],
        ContainerType.MEDIUM: [
            '{} looks rummages through an abandoned rucksack and finds a {}!',
        ],
        ContainerType.LARGE: [
            '{} uncovers a {} from underneath a poorly camouflaged tarpaulin. someone left in a hurry!',
        ]  
    }
}

afterFightSearchables = {
    'fail': [ #.format(charName)
        '{} finds no bodies here.'
    ],
    'succeed': [ #.format(charName,bodyList)
        '{} sees the remains of {} slumped on the ground.'
    ]
}
