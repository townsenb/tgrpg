import random
import sys
import traceback
import time
import datetime

from telegram import InlineKeyboardButton, InlineKeyboardMarkup, user
from telegram.ext import Updater, CommandHandler, MessageHandler, CallbackQueryHandler
import telegram.message

from datetime import datetime, timedelta

import gameManager
import Context

print("starting...")

#BOT TOKEN
token = '1933899806:AAHXUoaFh8PpAWNvhr4_lmHpqgizi-cUBh0'
bot = telegram.Bot(token=token)

global currentLoc

#private message or tegramalon chat
TEST_MODE = True
chat_id = 517424229 # - private chat
if not TEST_MODE:
    chat_id = chat_id = -1001552769538 # - Public


gm = gameManager.GameManager(chat_id,bot,TEST_MODE)


#send a message
def text(string):
    global chat_id
    if gm.test_mode:
        print(f"\n{('_'*80)}\n{string}\n{('_'*80)}\n")
    else:
        bot.sendMessage(text=string,chat_id=chat_id)


#ensure character is made before most commands
def charCheck(update,context):
    if update.message.from_user.first_name not in gm.playerDict:
        text("make a character first please!")
        return False
    return True

#test
def yesNo(update, string):
    keyboard = [[
            InlineKeyboardButton("Yes", callback_data='1'),
            InlineKeyboardButton("No", callback_data='0'),
    ]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text(string, reply_markup=reply_markup)

#test
def button(update, context):
    """Parses the CallbackQuery and updates the message text."""
    query = update.callback_query

    # CallbackQueries need to be answered, even if no notification to the user is needed
    # Some clients may have trouble otherwise. See https://core.telegram.org/bots/api#callbackquery
    query.answer()

    query.edit_message_text(text=f"Selected option: {query.data}")


# change name of chat to "Tegramalon -" and whatever world was just made
def setChatName():
    if chat_id == -1001552769538:
        newchat = f'Tegramalon - {gm.world.name}'
        bot.setChatTitle(chat_id,newchat)


def last(update,context):
    if not charCheck(update,context):
        return
    player = gm.playerDict[update.message.from_user.first_name]
    dm(update,context,player.lastCmd,player.lastArgs)
    return

# All interaction is routed thru /dm command
def dm(update,context,prev=None,prevArgs=None):
    name = prev
    args = prevArgs
    if prevArgs is None:
        args = context.args
    else:
        args.insert(0,name)
    if prev is None:
        name = args[0].lower()

    print(f"{name} {args}")

    # easter egg for sam :)
    if args == ['I','check','my','casio','calculator','watch']:
        text(f"It's currently {datetime.now().strftime('%H:%M')}, you've way overslept!")
        return

    # print command and accompanying arguments
    gm.log(f"cmd: {name}")
    for arg in args:
        gm.log(f"\t{arg}")

    
    #order of commands is important

    # create a new character - must be done to do most commands
    if name == 'newcharacter':
        if len(args) < 2:
            text("Try again with a name.")
            return
        if len(args) == 2:
            gm.newCharacter(update.message.from_user.first_name,args[1])
        elif len(args) == 3:
            gm.newCharacter(update.message.from_user.first_name,args[1],args[2])
        return
    
    # list commands - important to usable before any other commands
    if name == 'help':
        param = None if len(args) < 2 else args[1]
        text(gm.help(cmd=param))
        return

    if name == 'changelog':
        text(gm.changes(None))
        return

    if name == 'log':
        strs = ['in','out']
        if len(args) != 2:
            text("Incorrect usage!\n/dm log [in | out]")
            return
        if args[1] not in strs:
            text("Incorrect usage!\n/dm log [in | out]")
            return
        text(gm.logger(update.message.from_user.first_name,args[1]))
        return

    if update.message.from_user.first_name in gm.loggedout:
        text("you are currently logged out. Log back in to resume playing")
        return

    # check if user has a character made
    if not charCheck(update,context):
        return
    
    # user entered a bad command
    if name not in gm.commands:
        text("unrecognized command")
        return

    # user entered a command that can't be accessed while in battle
    if gm.inBattle and name not in gm.battleCommands:
        text("You're in battle right now!")
        return

    if not gm.inBattle and name in gm.battleOnlyCommands:
        text("You can only use this command in battle")
        return

    char = gm.playerDict[update.message.from_user.first_name]
    cmd = gm.commands[name]

    # ensure proper amount of arguments was sent
    if len(args)-1 not in cmd['argc']:
        text(f"incorrect usage.\n{name} - {cmd['usage']}")
        return
    
    # all other commands need a character as first parameter
    args.insert(1,char)

    # stop cheaters from acting out of turn
    if gm.inBattle:
        turntaker = gm.turnOrder[gm.turn]
        time_delta = gm.battleTimeout - int(time.time()) 
        turntaker = gm.turnOrder[gm.turn]
        if time_delta <= 0 and char != turntaker:
            gm.skipturn(turntaker)
        elif turntaker.name != char.name and name in gm.turnEnders:
            text(f"wait a sec, it's {turntaker.name}'s turn! ({time_delta}s left)")
            return
        
    # set last command and arguments
    char.lastCmd = name
    char.lastArgs = args[2:] if len(args) > 2 else []

    # call specified function listed in the commands list and send the output text to chat
    output = cmd['fn'](*args[1:])
    if output != '' and output != None:
        text(output)


def tui_input():
    string = input("> ")
    c = Context.Context()
    c.args = c.tokenize(string)
    print(c.args)
    return c

# catch all errors to spit at telegram
try:

    # Create new world on startup
    gm.newCampaign()

    while gm.test_mode:
        context = tui_input()
        update = Context.Update()
        dm(update, context)


    updater = Updater(token, use_context=True)

    updater.dispatcher.add_handler(CommandHandler('dm', dm, pass_args=True))
    updater.dispatcher.add_handler(CommandHandler('last', last, pass_args=True))

    updater.dispatcher.add_handler(CallbackQueryHandler(button))


    updater.start_polling()
    print("DM Running!")
    updater.idle()
except:
    text(f"ERROR\n\nTraceback:\n{traceback.format_exc()}")
    text(f"exiting... say goodbye to the world of {gm.world.name}\nyell at Ben")
    exit()

