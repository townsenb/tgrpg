import random
import armor
from character import Character
import item
from weapon import Weapon
import weapon
import namebits
import nameGen

def randomItem(owner: Character) -> item.Item:
    RARITY_MAX = 100
    rarityWeights = [(RARITY_MAX - i.rarity) for i in item.items.values()]
    tmpItem = item.items[random.choices(list(item.items.keys()),rarityWeights,k=1)[0]]
    randchoice = random.randint(0,4) 
    if randchoice == 3:
        rarityWeights = [(RARITY_MAX - w.rarity) for w in weapon.weapons.values()]
        tmpItem = weapon.weapons[random.choices(list(weapon.weapons.keys()),rarityWeights,k=1)[0]]
    elif randchoice == 4:
        rarityWeights = [(RARITY_MAX - a.rarity) for a in armor.armors.values()]
        tmpItem = armor.armors[random.choices(list(armor.armors.keys()),rarityWeights,k=1)[0]]
        
    tmpItem.owner = owner
    if owner is not None: 
        if type(tmpItem) == Weapon:
            owner.weapons.append(tmpItem)
        else:
            owner.items.append(tmpItem)
    tmpItem.smushName = tmpItem.getSmushName()
    return tmpItem

def intersection(lista, listb):
    result = []
    for element in lista:
        if element in listb:
            result.append(element)
    return result
